<?php

namespace App\Http\Controllers;

use App\Arco\Zones\Unit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UnitsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $units = Unit::with("zone", "type")->get();

        return $this->jsonResponse("Success", $units);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate
        $validator = Validator::make($request->all(), [
            "name" => "required",
            "zone_id" => "required",
            "type_id" => "required"
        ]);

        if ($validator->fails()) {
            return $this->errorResponse("Invalid Data", "invalid data", $validator->errors(), 422);
        }

        $unit = Unit::create($request->all());

        return $this->jsonResponse("Success", $unit->load("zone", "type"));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $unit = Unit::findOrFail($id);

        return $this->jsonResponse("Success", $unit);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $unit = Unit::findOrFail($id);

        // validate
        $validator = Validator::make($request->all(), [
            "name" => "required",
            "zone_id" => "required",
            "type_id" => "required"
        ]);

        if ($validator->fails()) {
            return $this->errorResponse("Invalid Data", "invalid data", $validator->errors(), 422);
        }

        $unit->name = $request->name;
        $unit->description = $request->description;
        $unit->zone_id = $request->zone_id;
        $unit->type_id = $request->type_id;
        $unit->save();

        return $this->jsonResponse("Success", $unit);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $unit = Unit::findOrFail($id);

        $unit->delete();

        return $this->jsonResponse("Success");
    }

    public function bulkDelete(Request $request)
    {
        // get zones
        $units = Unit::whereIn("id", $request->ids)->get();

        // filter units
        $filtered = $units->filter(function ($unit)
        {
            return $unit->contracts->count() == 0; 
        });

        $exceptions = ($units->count() != $filtered->count());

        $deleted_ids = $filtered->pluck("id")->toArray();

        Unit::whereIn("id", $deleted_ids)->delete();

        return $this->jsonResponse("Success", [
            "exceptions" => (int)$exceptions,
            "deleted_ids" => $deleted_ids
        ]); 
    }
}
