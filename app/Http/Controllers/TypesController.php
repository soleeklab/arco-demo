<?php

namespace App\Http\Controllers;

use App\Arco\Zones\Type;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $types = Type::all();

        return $this->jsonResponse("Success", $types);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate
        $validator = Validator::make($request->all(), [
            "name" => "required",
            "category" => "required"
        ]);

        if ($validator->fails()) {
            return $this->errorResponse("Invalid Data", "invalid data", $validator->errors(), 422);
        }

        $type = Type::create($request->all());

        return $this->jsonResponse("Success", $type);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $type = Type::findOrFail($id);

        return $this->jsonResponse("Success", $type);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // validate
        $validator = Validator::make($request->all(), [
            "name" => "required",
            "category" => "required"
        ]);

        if ($validator->fails()) {
            return $this->errorResponse("Invalid Data", "invalid data", $validator->errors(), 422);
        }

        $type = Type::findOrFail($id);
        $type->name = $request->name;
        $type->description = $request->description;
        $type->category = $request->category;
        $type->save();

        return $this->jsonResponse("Success", $type);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $type = Type::findOrFail($id);

        // if($type->units->count()){
        //     return $this->errorResponse("Cannot Delete Type with Units", "Cannot Delete Type with Units", [], 400);
        // }

        $type->delete();

        return $this->jsonResponse("Success");
    }

    public function bulkDelete(Request $request)
    {
        // get types
        $types = Type::whereIn("id", $request->ids)->get();

        // filter types
        $filtered = $types->filter(function ($zone)
        {
            return $zone->units->count() == 0 || $zone->facilities->count() == 0; 
        });

        $exceptions = ($types->count() != $filtered->count());

        $deleted_ids = $filtered->pluck("id")->toArray();

        Type::whereIn("id", $deleted_ids)->delete();

        return $this->jsonResponse("Success", [
            "exceptions" => (int)$exceptions,
            "deleted_ids" => $deleted_ids
        ]); 
    }
}
