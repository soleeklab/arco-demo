<?php

namespace App\Http\Controllers;

use App\Arco\Issues\IssueCategory;
use App\Arco\Issues\IssueLevel;
use App\Arco\Issues\IssueMode;
use App\Arco\Issues\IssueStatus;
use App\Arco\Issues\IssueType;
use App\Arco\Issues\IssueUrgency;
use App\Arco\Users\Country;
use Illuminate\Http\Request;

class ResourcesController extends Controller
{
    
    public function countries()
    {
    	$countries = Country::orderBy("name", "ASC")->get(["id", "name"]);

    	return $this->jsonResponse("Success", $countries);
    }

    public function issues_resources()
    {
    	$issue_statuses = IssueStatus::all();
    	$issue_levels = IssueLevel::all();
    	$issue_modes = IssueMode::all();
    	$issue_urgencies = IssueUrgency::all();
    	$issue_categories = IssueCategory::all();
    	$issue_types = IssueType::all();

    	return $this->jsonResponse("Success", [
    		"issue_statuses" => $issue_statuses,
    		"issue_levels" => $issue_levels,
    		"issue_modes" => $issue_modes,
    		"issue_urgencies" => $issue_urgencies,
    		"issue_categories" => $issue_categories,
    		"issue_types" => $issue_types
    	]);
    }
}
