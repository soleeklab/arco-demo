<?php

namespace App\Http\Controllers\Api;

use App\Arco\Billing\Bill;
use App\Arco\Billing\Service;
use App\Arco\News\Post;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    
    public function newFeed()
    {
    	$posts = Post::orderBy("created_at", "DESC")->take(10)->get();

    	return $this->jsonResponse("Success", $posts);
    }

    public function myBills()
    {
    	$user = \Auth::user();

        if($user->isLinked()){
            // get parent account
            $user = $user->parent;
        }
        
    	// dd($user);
    	$services = Service::with(["bills" => function ($q) use ($user)
        {
            return $q->where("resident_id", $user->id)->orderBy("created_at", "DESC");
        }])->get();

    	return $this->jsonResponse("Success", $services);
    }
    
    public function payBill(Request $request, $bill_id)
    {
    	// validate
        $validator = Validator::make($request->all(), [
            "image" => "required"
        ]);

        if ($validator->fails()) {
            return $this->errorResponse("Invalid Data", "invalid data", $validator->errors(), 422);
        }

        $bill = Bill::findOrFail($bill_id);

        $image = base64_decode($request->image);
        $rnd = str_random(6);
        $path = "bills/bills-{$rnd}.jpg";
        Storage::disk("public")->put($path, $image);
        $bill->image = "storage/".$path;
        $bill->status = 2;
        $bill->save();

        return $this->jsonResponse("Success", true);
    }

    public function editProfile(Request $request)
    {
    	// validate
        $validator = Validator::make($request->all(), [
            "name" => "required"
        ]);

        if ($validator->fails()) {
            return $this->errorResponse("Invalid Data", "invalid data", $validator->errors(), 422);
        }

        $user = \Auth::user();
        $user->username = $request->name;
        $user->email = $request->email;
        $user->birthdate = $request->birthdate;
        $user->save();

        if($request->image){
            $image = base64_decode($request->image);
            $rnd = str_random(6);
            $path = "profiles/profile-{$rnd}.jpg";
            Storage::disk("public")->put($path, $image);
            $user->image = "storage/".$path;
            $user->save();
        }

        return $this->jsonResponse("Success", $user);
    }

    public function changePassword(Request $request)
    {
    	// validate
        $validator = Validator::make($request->all(), [
            "old_password" => "required",
            "password" => "required",
            "confirm_password" => "required|same:password"
        ]);

        if ($validator->fails()) {
            return $this->errorResponse("Invalid Data", "invalid data", $validator->errors(), 422);
        }

        $user = \Auth::user();

        if(! Hash::check($request->old_password, $user->password)){
            return $this->errorResponse("Incorrect Password", "Incorrect Password", $validator->errors(), 422);
        }

        $user->password = bcrypt($request->password);
        $user->save();

        return $this->jsonResponse("Success", true);
    }
}
