<?php

namespace App\Http\Controllers\Api;

use App\Arco\Users\Inquiery;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class InquieryController extends Controller
{
    
    public function store(Request $request)
    {
    	$validator = Validator::make($request->all(), [
            "name" => "required",
            "phone" => "required",
            "description" => "required"
        ]);

        if ($validator->fails()) {
            return $this->errorResponse("Invalid Data", "invalid data", $validator->errors(), 422);
        }

        $inq = Inquiery::create($request->all());

        return $this->jsonResponse("Success", true);
    }
}
