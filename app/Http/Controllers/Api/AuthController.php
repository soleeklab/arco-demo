<?php

namespace App\Http\Controllers\Api;

use App\Arco\Users\UsersRepository;
use App\Http\Controllers\Controller;
use App\Arco\Services\AccountService;
use App\Arco\Services\SmsService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller
{
    private $userRepo;
    private $smsService;
    private $accService;

    public function __construct(UsersRepository $userRepo, SmsService $smsService, AccountService $accService)
    {
        $this->userRepo = $userRepo;
        $this->smsService = $smsService;
        $this->accService = $accService;
    }

    
    public function login(Request $request)
    {
    	// validate
        $validator = Validator::make($request->all(), [
            "phone" => "required",
            "password" => "required"
        ]);

        if ($validator->fails()) {
            return $this->errorResponse("Phone Number already used", "invalid data", $validator->errors(), 422);
        }

        // grab credentials from the request
        $credentials = $request->only('phone', 'password');

        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return $this->errorResponse("Invalid credentials", "invalid data", [], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return $this->errorResponse("Invalid credentials", "invalid data", [], 401);
        }

    	$user = JWTAuth::user();
        $user->token = $token;
    	// return response
        return $this->jsonResponse("Success", $user);
    }

    public function adminLogin(Request $request)
    {
        // validate
        $validator = Validator::make($request->all(), [
            "email" => "required",
            "password" => "required"
        ]);

        if ($validator->fails()) {
            return $this->errorResponse("Invalid Data", "invalid data", $validator->errors(), 422);
        }

        // grab credentials from the request
        $credentials = $request->only('email', 'password');

        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return $this->errorResponse("Invalid credentials", "invalid data", [], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return $this->errorResponse("Invalid credentials", "invalid data", [], 401);
        }

        $user = JWTAuth::user();
        if(!$user->hasRole("admin") && !$user->hasRole("super_admin") ){
            return $this->errorResponse("Invalid Permissions", "invalid data", [], 403);
        }

        $user->token = $token;
        // return response
        return $this->jsonResponse("Success", $user);
    }

    public function register(Request $request)
    {
        // validate
        $validator = Validator::make($request->all(), [
            "phone" => "required|exists:users,phone",
        ]);

        if ($validator->fails()) {
            return $this->errorResponse("Phone number not registered, please contact Arco", "invalid data", $validator->errors(), 422);
        }


        $user = $this->userRepo->findByMobile($request->phone);

        if($user->confirmed){
            return $this->errorResponse("Phone number already registered", "invalid data", $validator->errors(), 422);   
        }
        
        if($user){
            $user = $this->accService->createVerificationCode($user);
            $msg = $this->smsService->buildVerificationMessage($user->verification_code);
            $this->smsService->sendSms($user->phone, $msg);
        }

        return $this->jsonResponse("Success", true);
    }

    public function confirm(Request $request)
    {
        // validate
        $validator = Validator::make($request->all(), [
            "phone" => "required|exists:users,phone",
            "code" => "required"
        ]);

        if ($validator->fails()) {
            return $this->errorResponse("Invalid data", "invalid data", $validator->errors(), 422);
        }


        $user = $this->userRepo->findByMobile($request->phone);

        if($user->verification_code != $request->code){
            return $this->errorResponse("Invalid code", "invalid data", false, 401);
        }

        return $this->jsonResponse("Success", $user);
    }

    public function resend(Request $request)
    {
        // validate
        $validator = Validator::make($request->all(), [
            "phone" => "required|exists:users,phone",
        ]);

        if ($validator->fails()) {
            return $this->errorResponse("Phone Number already used", "invalid data", $validator->errors(), 422);
        }

        $user = $this->userRepo->findByMobile($request->phone);

        if($user){
            $user = $this->accService->createVerificationCode($user);
            $msg = $this->smsService->buildVerificationMessage($user->verification_code);
            $this->smsService->sendSms($user->phone, $msg);
        }

        return $this->jsonResponse("Success", true);
    }

    public function create(Request $request)
    {
        // validate
        $validator = Validator::make($request->all(), [
            "phone" => "required|exists:users,phone",
            "code" => "required",
            "password" => "required",
            "confirm_password" => "required|same:password",
            "username" => "required"
        ]);

        if ($validator->fails()) {
            return $this->errorResponse("Invalid data", "invalid data", $validator->errors(), 422);
        }

        $user = $this->userRepo->findByMobile($request->phone);

        if(!$this->accService->validateCode($user, $request->code)){
            return $this->errorResponse("Invalid code", "invalid data", false, 401);
        }

        $user->password = bcrypt($request->password);
        $user->username = $request->username;
        $user->birthdate = $request->birthdate;
        $user->email = $request->email;
        $user->confirmed = 1;
        $user->save();

        $token = JWTAuth::fromUser($user);
        $user->token = $token;

        return $this->jsonResponse("Success", $user);
    }

    public function forgetPassword(Request $request)
    {
        // validate
        $validator = Validator::make($request->all(), [
            "phone" => "required|exists:users,phone",
        ]);

        if ($validator->fails()) {
            return $this->errorResponse("Phone Number does not exist", "invalid data", $validator->errors(), 422);
        }

        $user = $this->userRepo->findByMobile($request->phone);

        if($user){
            $user = $this->accService->createVerificationCode($user);
            $msg = $this->smsService->buildVerificationMessage($user->verification_code);
            $this->smsService->sendSms($user->phone, $msg);
        }

        return $this->jsonResponse("Success", true);
    }

    public function forgetCode(Request $request)
    {
        // validate
        $validator = Validator::make($request->all(), [
            "phone" => "required|exists:users,phone",
            "code" => "required"
        ]);

        if ($validator->fails()) {
            return $this->errorResponse("Invalid data", "invalid data", $validator->errors(), 422);
        }


        $user = $this->userRepo->findByMobile($request->phone);

        if($user->verification_code != $request->code){
            return $this->errorResponse("Invalid code", "invalid data", false, 401);
        }

        return $this->jsonResponse("Success", true);
    }

    public function forgetConfirm(Request $request)
    {
        // validate
        $validator = Validator::make($request->all(), [
            "phone" => "required|exists:users,phone",
            "code" => "required",
            "password" => "required",
            "confirm_password" => "required|same:password"
        ]);

        if ($validator->fails()) {
            return $this->errorResponse("Invalid data", "invalid data", $validator->errors(), 422);
        }


        $user = $this->userRepo->findByMobile($request->phone);
        $user->password = bcrypt($request->password);
        $user->save();

        return $this->jsonResponse("Success", true);
    }
}
