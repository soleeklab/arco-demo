<?php

namespace App\Http\Controllers\Api;

use App\Arco\Issues\Issue;
use App\Arco\Issues\IssuesTransformer;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class IssuesController extends Controller
{
    private $issuesTrans;

    public function __construct(IssuesTransformer $issuesTrans)
    {
        $this->issuesTrans = $issuesTrans;
    }

    
    public function index()
    {
    	$issues = \Auth::user()->issues()->whereNull("parent_id")->get();

        return $this->jsonResponse("Success", $this->issuesTrans->transformCollection($issues->load("extended_issues")));
    }

    public function show($id)
    {
    	$issue = \Auth::user()->issues()->findOrFail($id);

        return $this->jsonResponse("Success", $issue->load("extended_issues"));
    }

    public function store(Request $request)
    {
        // validate
        $validator = Validator::make($request->all(), [
            "description" => "required",
            "type" => "required",
            "request_date" => "required",
            "request_time" => "required"
        ]);

        if ($validator->fails()) {
            return $this->errorResponse("Invalid Data", "invalid data", $validator->errors(), 422);
        }

        $issue = new Issue;
        $issue->description = $request->description;
        $issue->request_date = $request->request_date;
        $issue->request_time = $request->request_time;
        $issue->type = $request->type;
        $issue->user_id = \Auth::id();
        $issue->save();

        return $this->jsonResponse("Success", $issue);
    }

    public function confirm(Request $request, $id)
    {
        // validate
        $validator = Validator::make($request->all(), [
            "done" => "required",
            "reason" => "required_if:done,0"
        ]);

        if ($validator->fails()) {
            return $this->errorResponse("Invalid Data", "invalid data", $validator->errors(), 422);
        }

        $issue = Issue::findOrFail($id);
        $issue->done = $request->done;
        $issue->reason = $request->reason;
        $issue->rating = $request->rating;
        $issue->confirmed_at = date("Y-m-d H:i");
        $issue->save();

        return $this->jsonResponse("Success");
    }
}
