<?php

namespace App\Arco\Users;

class UsersRepository
{
	
	public function findByMobile($mobile)
	{
		return User::where("phone", $mobile)->get()->first();
	}

	public function findByRole($role)
	{
		return User::whereHas("roles", function ($q) use ($role)
		{
			return $q->where("name", $role);
		})->get();
	}

	public function getResidents()
	{
		return $this->findByRole("resident");
	}

	public function getResident($id)
	{
		return User::whereHas("roles", function ($q)
		{
			return $q->where("name", "resident");
		})->where("id", $id)->get()->first();
	}
}