<?php

namespace App\Arco\Services;

use Aloha\Twilio\Twilio;
use Illuminate\Support\Facades\Config;
use Twilio\Rest\Client;

/**
* 
*/
class SmsService
{

	private $twilio;

	public function __construct()
	{
		$this->twilio = new Client(Config::get('twilio.twilio.connections.twilio.sid'), Config::get('twilio.twilio.connections.twilio.token'));
		$this->fromNumber = Config::get('twilio.twilio.connections.twilio.from');
	}

	
	public function sendSms($phone_number, $message)
	{
		try{
			$this->twilio->messages->create($this->validatePhone($phone_number), ["from" => $this->fromNumber, "body" => $message]);
		}catch(\Exception $e){
			
		}
	}

	public function buildVerificationMessage(string $verification_code)
	{
		return "Your verification code is " . $verification_code;
	}

	private function validatePhone($phone)
	{
		// dd(preg_match('[^\+20|20]', $phone));
		if (preg_match('[^\+20|20]', $phone)) {
		    return $phone;
		} else {
		    return "+20" . $phone;
		}
	}
}