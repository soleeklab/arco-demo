<?php

namespace App\Arco\Zones;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    
    protected $fillable = ["name", "description", "category"];

    protected $appends = ["category_name", "units_count"];

    public function units()
    {
    	return $this->hasMany(Unit::class);
    }

    public function facilities()
    {
    	return $this->hasMany(Facility::class);
    }

    public function getUnitsCountAttribute()
    {
    	return $this->units->count() + $this->facilities->count();
    }

    public function getCategoryNameAttribute()
    {
    	if($this->attributes["category"] == 1){
    		return "Unit";
    	}else{
    		return "Facility";
    	}
    }
}
