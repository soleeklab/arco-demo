<?php

namespace App\Arco\Zones;

use App\Arco\Contracts\Contract;
use App\Arco\Zones\Type;
use App\Arco\Zones\Zone;
use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{

    protected $fillable = ["name", "description", "zone_id", "type_id"];

    public function zone()
    {
    	return $this->belongsTo(Zone::class);
    }

    public function type()
    {
    	return $this->belongsTo(Type::class);
    }

    public function contracts()
    {
    	return $this->hasMany(Contract::class);
    }
}
