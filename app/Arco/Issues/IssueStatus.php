<?php

namespace App\Arco\Issues;

use Illuminate\Database\Eloquent\Model;

class IssueStatus extends Model
{
    
    protected $fillable = ["name", "description"];
}
