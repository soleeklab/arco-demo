<?php

namespace App\Arco\Issues;

use Illuminate\Database\Eloquent\Model;

class IssueLevel extends Model
{
    
    protected $fillable = ["name", "description"];
}
