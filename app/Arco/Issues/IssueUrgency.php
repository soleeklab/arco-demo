<?php

namespace App\Arco\Issues;

use Illuminate\Database\Eloquent\Model;

class IssueUrgency extends Model
{
    
    protected $fillable = ["name", "description"];
}
