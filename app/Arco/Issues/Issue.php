<?php

namespace App\Arco\Issues;

use App\Arco\Issues\IssueState;
use App\Arco\Users\User;
use Illuminate\Database\Eloquent\Model;

class Issue extends Model
{
    
    protected $dates = [];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function assignee()
    {
        return $this->belongsTo(User::class, "assignee_id");
    }

    public function extended_issues()
    {
    	return $this->hasMany(self::class, "parent_id");
    }

    public function parent_issue()
    {
    	return $this->belongsTo(self::class, "parent_id");
    }

    public function issue_state()
    {
        return $this->belongsTo(IssueState::class, "state");
    }
}
