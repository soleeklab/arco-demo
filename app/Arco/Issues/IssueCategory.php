<?php

namespace App\Arco\Issues;

use Illuminate\Database\Eloquent\Model;

class IssueCategory extends Model
{
    
    protected $fillable = ["name", "description"];
}
