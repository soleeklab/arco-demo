<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIssuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('issues', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("user_id")->nullable();
            $table->text("description");
            $table->date("request_date")->nullable();
            $table->string("request_time")->nullable();
            $table->integer("issue_type_id")->nullable();
            $table->integer("issue_level_id")->nullable();
            $table->integer("issue_mode_id")->nullable();
            $table->integer("issue_status_id")->nullable();
            $table->integer("issue_category_id")->nullable();
            $table->integer("issue_urgency_id")->nullable();
            $table->integer("assignee_id")->nullable();
            $table->integer("state")->default(0);
            $table->string("type")->nullable();
            $table->integer("rating")->nullable();
            $table->integer("parent_id")->nullable();
            $table->datetime("confirmed_at")->nullable();
            $table->boolean("done")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('issues');
    }
}
