<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(["namespace" => "Api"], function ()
{

	Route::post("auth", "AuthController@login");
	Route::post("auth/signup", "AuthController@register");
	Route::post("auth/confirm", "AuthController@confirm");
	Route::post("auth/resend", "AuthController@resend");
	Route::post("auth/create", "AuthController@create");

	Route::post("auth/forget_password", "AuthController@forgetPassword");
	Route::post("auth/forget_password_code", "AuthController@forgetCode");
	Route::post("auth/forget_password_confirm", "AuthController@forgetConfirm");

	Route::post("inquiery", "InquieryController@store");

	Route::group(["middleware" => "jwt.auth"], function ()
	{
		Route::get("notifications", "NotificationsController@index");
		
		Route::get("my_bills", "ProfileController@myBills");
		Route::get("news", "ProfileController@newFeed");
		Route::post("bills/{id}/pay", "ProfileController@payBill");
		Route::post("auth/edit_profile", "ProfileController@editProfile");
		Route::post("auth/change_password", "ProfileController@changePassword");

		// issues
		Route::get("user/issues", "IssuesController@index");
		Route::get("user/issues/{id}", "IssuesController@show");
		Route::post("user/issues", "IssuesController@store");
		Route::post("user/issues/{id}/confirm", "IssuesController@confirm");

		// linked accounts
		Route::get("user/linked", "LinkedAccountsController@index");
		Route::post("user/linked", "LinkedAccountsController@store");
		Route::delete("user/linked/{id}", "LinkedAccountsController@unlinkAccount");
	});

});


Route::group(['prefix' => "admin"], function ()
{

	Route::get("countries", "ResourcesController@countries");
	Route::get("issues_resources", "ResourcesController@issues_resources");

	Route::post("auth", "Api\AuthController@adminLogin");

	Route::post("zones/bulk-delete", "ZonesController@bulkDelete");
	Route::post("zones/{id}", "ZonesController@update");
	Route::resource("zones", "ZonesController");

	Route::post("types/bulk-delete", "TypesController@bulkDelete");
	Route::post("types/{id}", "TypesController@update");
	Route::resource("types", "TypesController");

	Route::post("units/bulk-delete", "UnitsController@bulkDelete");
	Route::post("units/{id}", "UnitsController@update");
	Route::resource("units", "UnitsController");

	Route::post("facilities/bulk-delete", "FaciltiesController@bulkDelete");
	Route::post("facilities/{id}", "FaciltiesController@update");
	Route::resource("facilities", "FaciltiesController");

});